package dytt8

import (
	"bytes"
	"context"
	"encoding/xml"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"strings"
	"time"

	gohtml "golang.org/x/net/html"

	"gitlab.com/comphilip/dytt-rss/html"

	"github.com/spf13/cast"
	"golang.org/x/net/html/atom"
	"golang.org/x/text/encoding/simplifiedchinese"
	rss "golang.org/x/tools/blog/atom"

	"gitlab.com/comphilip/dytt-rss/logging"
)

type Link struct {
	Label string
	Href  string
	Date  time.Time
}

type Dytt8AtomService struct {
	fetchEntryHtmlChan chan string
	assetsDir          string
}

const DYTT8_URL = "https://www.dy2018.com/"

func NewDytt8AtomService(assetsDir string) *Dytt8AtomService {
	return &Dytt8AtomService{
		assetsDir:          assetsDir,
		fetchEntryHtmlChan: make(chan string, 1024),
	}
}

func (d *Dytt8AtomService) GenerateAtom(atomFileName string) error {
	html, err := d.fetIndexHtml()
	if err != nil {
		return err
	}
	links, err := d.extractMovieLinks(html)
	if err != nil {
		return err
	}
	feed, err := d.generateAtom(links)
	if err != nil {
		return err
	}
	data, err := xml.Marshal(feed)
	if err != nil {
		return logging.Error(err)
	}
	if err := ioutil.WriteFile(atomFileName, data, os.FileMode(0644)); err != nil {
		return logging.Error(err)
	}
	return nil
}

func (d *Dytt8AtomService) fetIndexHtml() (string, error) {
	url := DYTT8_URL
	ctx, cancel := context.WithTimeout(context.Background(), time.Minute)
	defer cancel()
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, url, nil)
	if err != nil {
		return "", logging.Error(err)
	}
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return "", logging.Error(err)
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		return "", logging.Errorf("%s response %d", url, resp.StatusCode)
	}
	decoder := simplifiedchinese.GBK.NewDecoder()
	html, err := ioutil.ReadAll(decoder.Reader(resp.Body))
	if err != nil {
		return "", logging.Error(err)
	}
	return string(html), nil
}

func (d *Dytt8AtomService) extractMovieLinks(dytt8Html string) ([]*Link, error) {
	node, err := gohtml.Parse(strings.NewReader(dytt8Html))
	if err != nil {
		return nil, logging.Error(err)
	}
	re := make([]*Link, 0, 1000)
	for _, movieListSection := range html.SelectAllDescendantNodes(node, &html.NodeMatch{
		TagName:   atom.Div,
		AttrName:  "class",
		AttrValue: "co_content222",
	}) {
		for _, li := range html.SelectAllDescendantNodes(movieListSection, &html.NodeMatch{TagName: atom.Li}) {
			a := html.FindFirstDescendantNode(li, &html.NodeMatch{TagName: atom.A})
			if a == nil {
				continue
			}
			var link *Link
			for _, attr := range a.Attr {
				if attr.Key == "href" &&
					strings.HasPrefix(attr.Val, "/i/") &&
					strings.HasSuffix(attr.Val, ".html") &&
					!strings.HasSuffix(attr.Val, "/index.html") {
					link = &Link{
						Href:  DYTT8_URL + attr.Val[1:], // skip / prefix
						Label: html.InnerText(a),
					}
				}
			}
			if link != nil {
				if dateNode := html.FindFirstDescendantNode(li, &html.NodeMatch{TagName: atom.Span}); dateNode != nil {
					dateStr := html.InnerText(dateNode)
					parts := strings.Split(dateStr, "-")
					year := time.Now().Year()
					if len(parts) == 2 {
						link.Date = time.Date(year, time.Month(cast.ToInt(parts[0])), cast.ToInt(parts[1]), 0, 0, 0, 0, time.UTC)
					} else {
						logging.Warnf("Invalid date: %s", dateStr)
					}
				}
				if link.Date.IsZero() {
					link.Date = time.Now()
				}
				re = append(re, link)
			}
		}
	}
	return re, nil
}

func (d *Dytt8AtomService) extractMovieDetailContent(detailHtml []byte) (string, error) {
	node, err := gohtml.Parse(bytes.NewReader(detailHtml))
	if err != nil {
		return "", logging.Error(err)
	}
	contentParentNode := html.FindFirstDescendantNode(node, &html.NodeMatch{
		TagName:   atom.Div,
		AttrName:  "id",
		AttrValue: "Zoom",
	})
	if contentParentNode != nil {
		w := bytes.NewBuffer(nil)
		if err := gohtml.Render(w, contentParentNode); err != nil {
			return "", logging.Error(err)
		}
		return w.String(), nil
	}
	return "", nil
}

func (d *Dytt8AtomService) generateAtom(links []*Link) (*rss.Feed, error) {
	feed := &rss.Feed{}
	feed.Author = &rss.Person{
		Name:  "comphilip",
		Email: "comphilip@msn.com",
	}
	feed.Title = "电影天堂v1.0.0"
	feed.ID = "urn:uuid:b70432d4-d37a-479f-b3f1-ccd3a805a188"
	feed.Updated = rss.Time(time.Now())
	feed.Link = []rss.Link{{Href: DYTT8_URL}}
	for _, link := range links {
		entry := rss.Entry{
			Title:     link.Label,
			ID:        link.Href,
			Link:      []rss.Link{{Href: link.Href}},
			Published: rss.Time(link.Date),
			Updated:   rss.Time(link.Date),
			Author:    &rss.Person{Name: DYTT8_URL},
			Summary: &rss.Text{
				Type: "text",
				Body: link.Label,
			},
			Content: &rss.Text{
				Type: "text",
				Body: link.Label,
			},
		}
		if html, err := d.getCachedEntryHtml(link.Href); err == nil && len(html) > 0 {
			entry.Content.Type = "html"
			if html, err := d.extractMovieDetailContent(html); err == nil && len(html) > 0 {
				entry.Content.Body = html
			} else {
				entry.Content.Body = string(html)
			}
		}
		feed.Entry = append(feed.Entry, &entry)
	}
	return feed, nil
}

func (d *Dytt8AtomService) cacheFileName(entryUrl string) (string, error) {
	parsedUrl, err := url.Parse(entryUrl)
	if err != nil {
		return "", logging.Error(err)
	}
	return strings.TrimLeft(strings.Replace(parsedUrl.Path, "/", "-", -1), "-"), nil
}

func (d *Dytt8AtomService) getCachedEntryHtml(entryUrl string) ([]byte, error) {
	fileName, err := d.cacheFileName(entryUrl)
	if err != nil {
		return nil, err
	}
	cacheFileName := filepath.Join(d.assetsDir, fileName)
	if _, err := os.Stat(cacheFileName); err == nil {
		if data, err := ioutil.ReadFile(cacheFileName); err != nil {
			return nil, logging.Error(err)
		} else {
			return data, nil
		}
	} else {
		select {
		case d.fetchEntryHtmlChan <- entryUrl:
		default:
		}
		return nil, nil
	}
}

func (d *Dytt8AtomService) fetchEntryHtml(entryUrl string) ([]byte, error) {
	fileName, err := d.cacheFileName(entryUrl)
	if err != nil {
		return nil, err
	}
	cacheFileName := filepath.Join(d.assetsDir, fileName)
	if _, err := os.Stat(cacheFileName); err == nil {
		if data, err := ioutil.ReadFile(cacheFileName); err != nil {
			return nil, logging.Error(err)
		} else {
			return data, nil
		}
	}
	logging.Infof("Fetching %s", entryUrl)
	resp, err := http.DefaultClient.Get(entryUrl)
	if err != nil {
		return nil, logging.Error(err)
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		return nil, logging.Errorf("dytt8 responses %d", resp.StatusCode)
	}
	html, err := ioutil.ReadAll(simplifiedchinese.GBK.NewDecoder().Reader(resp.Body))
	if err != nil {
		return nil, logging.Error(err)
	}
	if err := ioutil.WriteFile(cacheFileName, html, os.FileMode(0644)); err != nil {
		logging.Warn(err)
	}
	return html, nil
}

func (d *Dytt8AtomService) deleteOldCacheHtmls(freshCacheFiles []string) {
	htmlFiles, err := filepath.Glob(d.assetsDir + "/*.html")
	if err != nil {
		logging.Error(err)
		return
	}
	for _, f := range htmlFiles {
		name := filepath.Base(f)
		keep := false
		for _, fresh := range freshCacheFiles {
			if name == fresh {
				keep = true
				break
			}
		}
		if !keep {
			if err := os.Remove(f); err != nil {
				logging.Error(err)
			}
		}
	}
}

func (d *Dytt8AtomService) updateFeedEntryContent(atomFileName, entryUrl string, html []byte) []string {
	feedXml, err := ioutil.ReadFile(atomFileName)
	if err != nil {
		return nil
	}
	var feed rss.Feed
	if err := xml.Unmarshal(feedXml, &feed); err != nil {
		return nil
	}
	cacheFileNames := make([]string, 0, len(feed.Entry))
	for _, entry := range feed.Entry {
		if len(entry.Link) > 0 {
			if cacheFileName, err := d.cacheFileName(entry.Link[0].Href); err == nil {
				cacheFileNames = append(cacheFileNames, cacheFileName)
			}
			if entry.Link[0].Href == entryUrl {
				entry.Content = &rss.Text{
					Type: "html",
					Body: string(html),
				}
				if html, err := d.extractMovieDetailContent(html); err == nil && len(html) > 0 {
					entry.Content.Body = html
				}
				feedXml, err = xml.Marshal(&feed)
				if err != nil {
					return nil
				}
				if err := ioutil.WriteFile(atomFileName, feedXml, os.FileMode(0644)); err != nil {
					logging.Error(err)
				}
			}
		}
	}
	return cacheFileNames
}

func (d *Dytt8AtomService) BackgroundFetchEntryHtml(atomFileName string) {
	for entryUrl := range d.fetchEntryHtmlChan {
		if html, err := d.fetchEntryHtml(entryUrl); err == nil {
			cacheFilesInUse := d.updateFeedEntryContent(atomFileName, entryUrl, html)
			if len(cacheFilesInUse) > 0 {
				d.deleteOldCacheHtmls(cacheFilesInUse)
			}
		}
		time.Sleep(time.Minute) // avoid rate limit
	}
}
