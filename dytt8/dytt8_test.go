package dytt8

import (
	"encoding/xml"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"testing"
	"time"

	"golang.org/x/text/encoding/simplifiedchinese"

	"github.com/stretchr/testify/require"
	rss "golang.org/x/tools/blog/atom"
)

func TestFetIndexHtml(t *testing.T) {
	if os.Getenv("TEST_READ_DYTT") != "true" {
		t.SkipNow()
	}
	svc := NewDytt8AtomService(os.TempDir())
	html, err := svc.fetIndexHtml()
	require.NoError(t, err)
	require.Contains(t, html, "迅雷电影资源")
}

func TestFetchEntryHtml(t *testing.T) {
	if os.Getenv("TEST_READ_DYTT") != "true" {
		t.SkipNow()
	}
	svc := NewDytt8AtomService(os.TempDir())
	html, err := svc.fetchEntryHtml("https://www.dy2018.com/i/101057.html")
	require.NoError(t, err)
	require.Greater(t, len(html), 0)
}

func TestGetCachedEntryHtml(t *testing.T) {
	svc := NewDytt8AtomService(os.TempDir())
	testFileName := filepath.Join(os.TempDir(), "1-2.html")
	require.NoError(t, ioutil.WriteFile(testFileName, []byte("test"), os.FileMode(0644)))
	defer os.Remove(testFileName)

	data, err := svc.getCachedEntryHtml("https://dytt8.net/1/2.html")
	require.NoError(t, err)
	require.EqualValues(t, []byte("test"), data)
}

func TestExtractMovieLinks(t *testing.T) {
	svc := NewDytt8AtomService(os.TempDir())
	htmlData, err := ioutil.ReadFile("dy2018.html")
	require.NoError(t, err)
	html, err := simplifiedchinese.GBK.NewDecoder().Bytes(htmlData)
	require.NoError(t, err)
	links, err := svc.extractMovieLinks(string(html))
	require.NoError(t, err)
	require.Greater(t, len(links), 0)
	link := links[0]
	require.NotZero(t, link.Date)
	require.NotZero(t, link.Label)
	require.True(t, strings.HasPrefix(link.Href, "https://www.dy2018.com/i/"))
}

func TestGenerateAtom(t *testing.T) {
	svc := NewDytt8AtomService(os.TempDir())
	feed, err := svc.generateAtom([]*Link{
		{Label: "test1", Href: "http://1.html", Date: time.Now()},
		{Label: "test2", Href: "http://2.html", Date: time.Now()},
	})
	require.NoError(t, err)
	require.NotNil(t, feed)
	require.Len(t, feed.Entry, 2)
}

func TestUpdateFeedEntryContent(t *testing.T) {
	svc := NewDytt8AtomService(os.TempDir())
	feed, err := svc.generateAtom([]*Link{
		{Label: "test1", Href: "http://1.html", Date: time.Now()},
		{Label: "test2", Href: "http://2.html", Date: time.Now()},
	})
	require.NoError(t, err)
	xmlData, err := xml.Marshal(feed)
	require.NoError(t, err)
	atomFile := filepath.Join(os.TempDir(), "test.atom")
	require.NoError(t, ioutil.WriteFile(atomFile, xmlData, os.FileMode(0644)))
	defer os.Remove(atomFile)

	fileNames := svc.updateFeedEntryContent(atomFile, "http://1.html", []byte("fake html"))
	require.Len(t, fileNames, 2)

	xmlData, err = ioutil.ReadFile(atomFile)
	require.NoError(t, err)
	var savedFeed rss.Feed
	require.NoError(t, xml.Unmarshal(xmlData, &savedFeed))
	feedContentUpdated := false
	for _, entry := range savedFeed.Entry {
		if entry.Link[0].Href == "http://1.html" {
			require.Equal(t, "html", entry.Content.Type)
			require.Equal(t, "fake html", entry.Content.Body)
			feedContentUpdated = true
		}
	}
	require.True(t, feedContentUpdated)
}

func TestDeleteOldCacheHtmls(t *testing.T) {
	svc := NewDytt8AtomService(os.TempDir())
	require.NoError(t, exec.Command("touch", filepath.Join(os.TempDir(), "1.html")).Run())
	require.NoError(t, exec.Command("touch", filepath.Join(os.TempDir(), "2.html")).Run())

	defer os.Remove(filepath.Join(os.TempDir(), "1.html"))
	defer os.Remove(filepath.Join(os.TempDir(), "2.html"))

	svc.deleteOldCacheHtmls([]string{"1.html"})
	_, err := os.Stat(filepath.Join(os.TempDir(), "1.html"))
	require.NoError(t, err)
	_, err = os.Stat(filepath.Join(os.TempDir(), "2.html"))
	require.Error(t, err)
}

func TestExtractMovieDetailContent(t *testing.T) {
	htmlData, err := ioutil.ReadFile("detail.html")
	require.NoError(t, err)
	html, err := simplifiedchinese.GBK.NewDecoder().Bytes(htmlData)
	require.NoError(t, err)
	svc := NewDytt8AtomService(os.TempDir())
	detail, err := svc.extractMovieDetailContent(html)
	require.NoError(t, err)
	require.Contains(t, detail, "查尔斯·丹斯")
}
