package html

import (
	"bytes"
	"io/ioutil"
	"testing"

	"golang.org/x/net/html/atom"

	"github.com/stretchr/testify/require"
	"golang.org/x/net/html"
)

func readTestHtml(t *testing.T) *html.Node {
	htmlData, err := ioutil.ReadFile("./test.html")
	require.NoError(t, err)
	node, err := html.Parse(bytes.NewReader(htmlData))
	require.NoError(t, err)
	return node
}

func TestFindFirstDescendantNode(t *testing.T) {
	node := readTestHtml(t)
	p := FindFirstDescendantNode(node, &NodeMatch{
		TagName: atom.P,
	})
	require.NotNil(t, p)
	require.Equal(t, "1", InnerText(p))

	p = FindFirstDescendantNode(node, &NodeMatch{
		TagName:   atom.P,
		AttrName:  "class",
		AttrValue: "t",
	})
	require.NotNil(t, p)
	require.Equal(t, "3", InnerText(p))
}

func TestSelectAllDescendantNodes(t *testing.T) {
	node := readTestHtml(t)
	ps := SelectAllDescendantNodes(node, &NodeMatch{
		TagName: atom.P,
	})
	require.Len(t, ps, 3)

	ps = SelectAllDescendantNodes(node, &NodeMatch{
		TagName:   atom.P,
		AttrName:  "class",
		AttrValue: "t",
	})
	require.Len(t, ps, 1)
}
