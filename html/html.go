package html

import (
	"golang.org/x/net/html"
	"golang.org/x/net/html/atom"
)

type NodeMatchMode int8

type NodeMatch struct {
	TagName   atom.Atom
	AttrName  string
	AttrValue string
}

func FindFirstDescendantNode(node *html.Node, match *NodeMatch) *html.Node {
	child := node.FirstChild
	for child != nil {
		if child.Type == html.ElementNode {
			if child.DataAtom == match.TagName {
				if match.AttrName != "" {
					for _, attr := range child.Attr {
						if attr.Key == match.AttrName && attr.Val == match.AttrValue {
							return child
						}
					}
				} else {
					return child
				}
			}
			if re := FindFirstDescendantNode(child, match); re != nil {
				return re
			}
		}
		child = child.NextSibling
	}
	return nil
}

func SelectAllDescendantNodes(node *html.Node, match *NodeMatch) []*html.Node {
	var re []*html.Node
	child := node.FirstChild
	for child != nil {
		if child.Type == html.ElementNode {
			if child.DataAtom == match.TagName {
				if match.AttrName != "" {
					for _, attr := range child.Attr {
						if attr.Key == match.AttrName && attr.Val == match.AttrValue {
							re = append(re, child)
						}
					}
				} else {
					re = append(re, child)
				}
			}
			re = append(re, SelectAllDescendantNodes(child, match)...)
		}
		child = child.NextSibling
	}
	return re
}

func InnerText(node *html.Node) string {
	re := ""
	child := node.FirstChild
	for child != nil {
		if child.Type == html.ElementNode {
			re += InnerText(child)
		} else if child.Type == html.TextNode {
			re += child.Data
		}
		child = child.NextSibling
	}
	return re
}
