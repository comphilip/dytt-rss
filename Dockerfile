FROM alpine
WORKDIR /app
COPY dytt-rss ./
RUN apk add --no-cache ca-certificates tzdata
EXPOSE 8080
VOLUME ["/app/assets"]
entrypoint ["/app/dytt-rss"]
