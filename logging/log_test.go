package logging

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestLog(t *testing.T) {
	Debug("Debug msg")
	Info("Information msg")
	require.NotNil(t, Warn("Warning msg"))
	require.NotNil(t, Error("Error msg"))
	SetLevel(DEBUG)
	Debug("Debug msg again")

	require.Contains(t, Warnf("hello %s", "world").Error(), "hello world")
}

func TestParseLevel(t *testing.T) {
	require.Equal(t, DEBUG, ParseLevel("DEBUG"))
	require.Equal(t, INFO, ParseLevel("INFO"))
	require.Equal(t, WARN, ParseLevel("WARN"))
	require.Equal(t, ERROR, ParseLevel("ERROR"))
}
