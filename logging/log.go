package logging

import (
	"errors"
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"

	"github.com/labstack/echo/v4"
)

type Level int8

const (
	DEBUG Level = iota + 1
	INFO
	WARN
	ERROR
)

var (
	level = INFO
	debug = log.New(os.Stderr, "[DEBUG] ", log.Lshortfile)
	info  = log.New(os.Stderr, "[INFO] ", log.Lshortfile)
	warn  = log.New(os.Stderr, "[WARN] ", log.Lshortfile)
	err   = log.New(os.Stderr, "[ERROR] ", log.Lshortfile)
)

func ParseLevel(level string) Level {
	switch strings.ToLower(level) {
	case "debug":
		return DEBUG
	case "info":
		return INFO
	case "warn":
		return WARN
	case "error":
		return ERROR
	default:
		return INFO
	}
}

func SetLevel(l Level) {
	level = l
}

func Debug(v ...interface{}) {
	if level <= DEBUG {
		debug.Output(2, fmt.Sprint(v...))
	}
}

func Debugf(format string, v ...interface{}) {
	if level <= DEBUG {
		debug.Output(2, fmt.Sprintf(format, v...))
	}
}

func Info(v ...interface{}) {
	if level <= INFO {
		info.Output(2, fmt.Sprint(v...))
	}
}

func Infof(format string, v ...interface{}) {
	if level <= INFO {
		info.Output(2, fmt.Sprintf(format, v...))
	}
}

func Warn(v ...interface{}) error {
	msg := fmt.Sprint(v...)
	if level <= WARN {
		warn.Output(2, msg)
	}
	return echo.NewHTTPError(http.StatusBadRequest, msg)
}

func Warnf(format string, v ...interface{}) error {
	msg := fmt.Sprintf(format, v...)
	if level <= WARN {
		warn.Output(2, msg)
	}
	return echo.NewHTTPError(http.StatusBadRequest, msg)
}

func Error(v ...interface{}) error {
	msg := fmt.Sprint(v...)
	if level <= ERROR {
		err.Output(2, msg)
	}
	return errors.New(msg)
}

func Errorf(format string, v ...interface{}) error {
	msg := fmt.Sprintf(format, v...)
	if level <= ERROR {
		err.Output(2, msg)
	}
	return errors.New(msg)
}
