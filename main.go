package main

import (
	"log"
	"net/http"
	"os"
	"time"

	"gitlab.com/comphilip/dytt-rss/dytt8"
	"gitlab.com/comphilip/dytt-rss/logging"

	"github.com/labstack/echo/v4"

	_ "net/http/pprof"
)

const (
	ASSETS_DIR = "./assets"
	ATOM_FILE  = ASSETS_DIR + "/dytt8.atom"
)

func main() {
	if err := os.MkdirAll(ASSETS_DIR, os.FileMode(0766)); err != nil {
		log.Panic(err)
	}
	svc := dytt8.NewDytt8AtomService(ASSETS_DIR)
	// go svc.BackgroundFetchEntryHtml(ATOM_FILE)
	go generateAtom(svc)
	e := echo.New()
	e.GET("/debug/pprof/*", echo.WrapHandler(http.DefaultServeMux))
	e.GET("/dytt8.atom", func(c echo.Context) error {
		feed, err := os.Open(ATOM_FILE)
		if err != nil {
			return err
		}
		defer feed.Close()
		return c.Stream(http.StatusOK, "application/atom+xml", feed)
	})
	e.Logger.Fatal(e.Start(":8080"))
}

func generateAtom(svc *dytt8.Dytt8AtomService) {
	for {
		logging.Info("Generating dytt8 atom")
		if err := svc.GenerateAtom(ATOM_FILE); err == nil {
			logging.Info("dytt8 atom generated")
		}
		time.Sleep(time.Hour)
	}
}
