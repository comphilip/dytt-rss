module gitlab.com/comphilip/dytt-rss

go 1.13

require (
	github.com/labstack/echo/v4 v4.5.0
	github.com/spf13/cast v1.4.1
	github.com/stretchr/testify v1.7.0
	golang.org/x/net v0.0.0-20210908191846-a5e095526f91
	golang.org/x/text v0.3.7
	golang.org/x/tools v0.1.5
)
